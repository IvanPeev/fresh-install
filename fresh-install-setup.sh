#!/bin/bash
# NOTICE: only to be used for Manjaro XFCE
# WARNING: enable pikaur first before running - deprecated
# WARNING: use pamac instead
# TODO after setup:
# - browser:
#   - yt / hotmail / gmail login
#   - ublock origin
#   - https everywhere
#   - autoscroll
#   - scrollbar customizer
#   - allow all extensions private tab
#   - standard search engine start page
#   - set as standard browser
# - steam
# - telegram
# - slack
# - discord
# - vscode
# - boostnote
# - vital synth
# - binance
# - terminal color
# - add ssh public key where needed (azure)
# - import gpg email keys
# - add preferred programs to run at startup
# - configure screensaver settings
# - add desktop background

NAME="Ivan Peev";
EMAIL="i.ivanov.peev@hotmail.com";
PCM_PACKAGES="telegram-desktop code ttf-dejavu nodejs node-gyp npm paper-icon-theme-git arc-gtk-theme mpv gnome-calculator transmission-gtk python-pipenv python-pylint wine lib32-giflib lib32-libpng lib32-libldap lib32-gnutls lib32-mpg123 lib32-openal lib32-v4l-utils lib32-libjpeg-turbo lib32-libxcomposite lib32-libxinerama opencl-icd-loader lib32-opencl-icd-loader lib32-libxslt lib32-gst-plugins-base-libs dosbox simple-scan neofetch flashplugin shellcheck net-tools nmap calibre xscreensaver vmpk noto-fonts-emoji pantheon-calendar";
YRT_PACKAGES="slack-desktop discord google-chrome sublime-text-dev nvm xfce4-volumed brave-bin mongodb-bin mongodb-tools-bin staruml nudoku-git postman-bin boostnote-bin ungoogled-chromium ttf-fixedsys-excelsior-linux vital-synth binance spotify figma-linux";
RMV_PCM="vlc thunderbird galculator-gtk2 pavucontrol pulseaudio pulseaudio-alsa manjaro-pulse pulseaudio-ctl";
NPM_PACKAGES="npm node-gyp semver nodemon @angular/cli strongloop"

echo "Fresh install setup start...";
echo -n "Password: ";
read -s PASS;
echo $PASS | sudo -S clear;

echo "Updating packages...";
sleep 2;
sudo pacman -Syu;

echo "Removing unneccesary packages...";
sleep 2;
sudo pacman -R $RMV_PCM;

if [ -n $HOME/.ssh ]; then
	echo "Generating SSH keys...";
	sleep 2;
	ssh-keygen -C $EMAIL -b 4096;
fi

echo "Set git global configs...";
sleep 2;
git config --global user.name $NAME;
git config --global user.email $EMAIL;

if [ -n /media/mdrive ]; then
	DRIVE_EXISTS=$(lsblk | grep sdb);
	if [ ! -z "$DRIVE_EXISTS" ]; then
		echo "Set automatic mount mdrive...";
		sleep 2;
		sudo mkdir /media;
		sudo chown $USER /media;
		sudo mkdir /media/mdrive;
		echo "UUID=$(lsblk -no UUID /dev/sdc1) /media/mdrive $(lsblk -no FSTYPE /dev/sdc1) defaults,noatime 0 2" | sudo tee --append /etc/fstab;
		sudo mount -a;
		ln -s /media/mdrive/ $HOME/mdrive;
	fi
fi

echo "Installing necessary programs...";
sleep 2;
sudo pacman -S $PCM_PACKAGES;
pamac install $YRT_PACKAGES;

echo "Changing default keyboard settings...";
sleep 2;
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>t" -s "xfce4-terminal";
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Alt>r" -s "sh -c 'xfce4-panel && xfwm4 --replace'" --create -t string;
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Shift><Alt>s" -s "systemctl suspend" --create -t string;
xfconf-query -c xfce4-keyboard-shortcuts -p "/commands/custom/<Primary><Shift><Delete>" -s "xscreensaver-command -l" --create -t string;
xfconf-query -c keyboards -p /Default/KeyRepeat/Delay -s 200 --create -t int;
xfconf-query -c keyboards -p /Default/KeyRepeat/Rate -s 200 --create -t int;
xfconf-query -c xsettings -p /Net/CursorBlink -s true;
xfconf-query -c xsettings -p /Net/CursorBlinkTime -s 200;

echo "Configuring terminal presets...";
sleep 2;
cp $HOME/.config/xfce4/terminal/terminalrc $HOME/.config/xfce4/terminal/terminalrc.bak;
echo "ColorBackground=#3f4551" >> $HOME/.config/xfce4/terminal/terminalrc;
echo "FontName=DejaVu Sans Mono 8" >> $HOME/.config/xfce4/terminal/terminalrc;
sed -i -e 's/MiscCursorBlinks=FALSE/MiscCursorBlinks=TRUE/g' $HOME/.config/xfce4/terminal/terminalrc;
sed -i -e 's/MiscMenubarDefault=TRUE/MiscMenubarDefault=FALSE/g' $HOME/.config/xfce4/terminal/terminalrc;
sed -i -e 's/MiscDefaultGeometry=80x24/MiscDefaultGeometry=115x25/g' $HOME/.config/xfce4/terminal/terminalrc;
sed -i -e 's/MiscCursorShape=TERMINAL_CURSOR_SHAPE_BLOCK/MiscCursorShape=TERMINAL_CURSOR_SHAPE_IBEAM/g' $HOME/.config/xfce4/terminal/terminalrc;

echo "Changing themes...";
sleep 2;
if [ -d "$HOME/mdrive" ]; then
	xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/backdrop-cycle-enable --create -t bool -s true;
	xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/backdrop-cycle-period --create -t int -s 0;
	xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/backdrop-cycle-timer --create -t uint -s 5;
	xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/backdrop-cycle-random-order --create -t bool -s false;
	sudo sed -i '/background/d' /etc/lightdm/lightdm-gtk-greeter.conf;
fi
xfconf-query -c xfwm4 -p /general/theme -s "Arc-Dark";
xfconf-query -c xfwm4 -p /general/button_layout -s "|HMC";
xfconf-query -c xfwm4 -p /general/title_alignment -s "left";
xfconf-query -c xfwm4 -p /general/title_font -s "Sans 8";
xfconf-query -c xsettings -p /Net/ThemeName -s "Arc-Dark";
xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark";
xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "Adwaita";
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/image-style -s 4;
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/color1 --create -t uint -t uint -t uint -t uint -s 607 -s 4156 -s 7703 -s 65535;

if [ -n $HOME/.npm-global ]; then
	echo "Setting custom npm configuration...";
	sleep 2;
	mkdir $HOME/.npm-global;
	mkdir $HOME/.npm-global/bin;
	npm config set prefix '~/.npm-global';
	echo "export PATH=~/.npm-global/bin:$PATH" >> $HOME/.zshrc;
fi

echo "Installing npm packages...";
sleep 2;
npm install -g $NPM_PACKAGES;

echo "Installing oh-my-zsh and theme...";
sleep 2;
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)";
sudo chsh -s $(grep /zsh$ /etc/shells | tail -1);
git clone "https://github.com/petermbenjamin/purity" purity;
cp purity/purity.zsh-theme $HOME/.oh-my-zsh/custom/themes/purity.zsh-theme;
rm -rf purity;
sed -i -e 's/ZSH_THEME="robbyrussell"/ZSH_THEME="purity"/g' $HOME/.zshrc;

echo "Rebooting...";
sleep 5;
reboot;
